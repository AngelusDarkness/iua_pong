﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalArea : MonoBehaviour {

	void OnTriggerEnter(Collider col) {
		if(col.CompareTag("Ball")) {
			GameManager.Instance.UpdateScore(tag);
		}
	}
}
