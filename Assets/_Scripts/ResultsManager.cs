﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultsManager : MonoBehaviour {

	[SerializeField]
	private Text _player1ScoreUI;
	[SerializeField]
	private Text _player2ScoreUI;

	// Use this for initialization
	void Awake () {
		_player1ScoreUI.text = string.Format("Jugador 1: {0}",Constants.PLAYER_1_SCORE);
		_player2ScoreUI.text = string.Format("Jugador 2: {0}",Constants.PLAYER_2_SCORE);
	}
}
