﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
	[SerializeField]
	private Rigidbody _rb;

	public void AddForce(Vector3 force) {
		_rb.AddForce(force, ForceMode.VelocityChange);
	}

	public void ResetPosition(Vector3 newPosition) {
		_rb.velocity = Vector3.zero;
		transform.position = newPosition;
	}

}
