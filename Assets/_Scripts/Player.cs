﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	public bool canStartGame = false;
	public Transform kickoffPoint;

	[SerializeField]
	private float _speed = 10f;
	[SerializeField]
	private float _forceMagnitude = 10f;
	[SerializeField]
	private float _limit = 2f;	
	[SerializeField]
	private bool _isLeft = false;

	
	// Update is called once per frame
	void Update () {
		if(_isLeft) {
			if(Input.GetKey(KeyCode.W)) 
				Up();
			if(Input.GetKey(KeyCode.S)) 
				Down();		
		} else {
			if(Input.GetKey(KeyCode.UpArrow))
				Up();
			if(Input.GetKey(KeyCode.DownArrow))
				Down();
		}		
	}

	void Up() {
		if(canStartGame) {
			canStartGame = false;
			GameManager.Instance.StartGame(tag);
		}
		
		Vector3 pos = transform.position;
		pos.y = Mathf.Clamp(pos.y + Time.deltaTime * _speed,-_limit,_limit-0.5f);
		transform.position = pos;
	}

	void Down() {
		if(canStartGame) {
			canStartGame = false;
			GameManager.Instance.StartGame(tag);
		}
		
		Vector3 pos = transform.position;
		pos.y = Mathf.Clamp(pos.y - Time.deltaTime * _speed,-_limit-0.5f,_limit);
		transform.position = pos;
	}


	void OnCollisionEnter(Collision col) {
		if(col.gameObject.CompareTag("Ball")) {
			Vector3 force = new Vector3(_forceMagnitude,_forceMagnitude,0);
			force.x = (_isLeft)?_forceMagnitude:-_forceMagnitude;
			Ball ball = col.gameObject.GetComponent<Ball>();
			ball.AddForce(force);
		}
	}
}
