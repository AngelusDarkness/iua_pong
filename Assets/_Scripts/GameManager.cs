﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	[SerializeField]
	private int _maxScore = 3;
	[SerializeField]
	private Ball ball;
	[SerializeField]
	private Player _player1;
	[SerializeField]
	private Player _player2;

	[SerializeField]
	private Text _player1ScoreUI;
	[SerializeField]
	private Text _player2ScoreUI;

	private int _scorePlayer1;
	private int _scorePlayer2;

	private static GameManager _instance;
	public static GameManager Instance {
		get {
				if(_instance == null) {
					_instance = GameObject.FindObjectOfType(typeof(GameManager)) as GameManager;

					if(_instance == null) {
						GameObject gameManager = new GameObject();
						gameManager.AddComponent<GameManager>();
					}
				}
			
			return _instance;
		}

		set {
			_instance = value;
		}
		
	} 
	// Use this for initialization
	void Start () {
		_scorePlayer1 = 0;
		_scorePlayer2 = 0;

		_player1.canStartGame = true;
	}
	

	public void UpdateScore(string tag) {
		if(tag.CompareTo("Player2GoalArea") == 0) {
			_scorePlayer1++;
			_player1ScoreUI.text = _scorePlayer1.ToString();  
			if(_scorePlayer1 == _maxScore)
				RestartGame();
			else {
				StartCoroutine(ResetGame(_player2));
			}
				
		} else {
			_scorePlayer2++;
			_player2ScoreUI.text = _scorePlayer2.ToString();
			if(_scorePlayer2 == _maxScore)
				RestartGame();
			else {
				StartCoroutine(ResetGame(_player1));
			}		
		}
	}

	//Player es quien debe sacar.
	IEnumerator ResetGame(Player player) {
		yield return new WaitForSeconds(2f);
		ball.ResetPosition(player.kickoffPoint.position);
		player.canStartGame = true;
	}

	public void StartGame(string tag) {
		if(tag.CompareTo("Player1") == 0)
				ball.AddForce(new Vector3(10f,10f,0));
			else
				ball.AddForce(new Vector3(-10f,-10f,0));
	}

	void RestartGame() {
	   Constants.PLAYER_1_SCORE = _scorePlayer1;
       Constants.PLAYER_2_SCORE = _scorePlayer2;
	   SceneManager.LoadScene("Results");
	}
}
